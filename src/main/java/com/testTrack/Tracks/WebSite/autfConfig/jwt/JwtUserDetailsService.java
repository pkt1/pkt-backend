package com.testTrack.Tracks.WebSite.autfConfig.jwt;
import com.testTrack.Tracks.WebSite.entity.User;
import com.testTrack.Tracks.WebSite.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService{
    public final UserService userService;

    public JwtUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userService.getUserByLogin(login);
        if(user == null){
            throw new UsernameNotFoundException("User with login: " + login + "not found");
        }
        return JwtUserBuilder.create(user);
    }
}
