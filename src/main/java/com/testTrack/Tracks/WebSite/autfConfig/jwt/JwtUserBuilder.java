package com.testTrack.Tracks.WebSite.autfConfig.jwt;

import com.testTrack.Tracks.WebSite.entity.User;

public final class JwtUserBuilder {
    public JwtUserBuilder(){
    }

    public static JwtUser create(User user){
        return new JwtUser(user.getId(), user.getLogin(), user.getPassword());
    }
}
