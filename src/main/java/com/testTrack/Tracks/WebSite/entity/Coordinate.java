package com.testTrack.Tracks.WebSite.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Coordinate {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    private double x;
    private double y;
    private double height;

    public Coordinate(double x, double y, double height) {
        this.x = x;
        this.y = y;
        this.height = height;
    }
}
