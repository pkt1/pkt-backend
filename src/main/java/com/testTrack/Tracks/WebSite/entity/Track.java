package com.testTrack.Tracks.WebSite.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Track {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String name;
    private String nearestTown;
    private String creationDate;
    private String description;
    private double length;
    private int numberDays;
    private int numberRestDays;
    private int category;

    @ManyToOne(fetch = FetchType.EAGER)
    private User owner;

    @ManyToMany
    @JoinTable(name = "tracks_coordinates",
            joinColumns = @JoinColumn(name = "track_id"),
            inverseJoinColumns = @JoinColumn(name = "coordinate_id"))
    private Collection<Coordinate> coordinates;

    @OneToMany(mappedBy = "track", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<ExtendedObstacle> extendedObstacles;

    @OneToMany(mappedBy = "track", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<LocalObstacle> localObstacles;

    @OneToMany(mappedBy = "track", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<Comment> comments;


}
