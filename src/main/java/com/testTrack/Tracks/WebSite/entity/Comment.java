package com.testTrack.Tracks.WebSite.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Comment{
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String creationDate;
    private String text;

    @ManyToOne(fetch = FetchType.EAGER)
    private User author;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Track track;

    @ManyToOne(fetch = FetchType.EAGER)
    private Comment comment;

    @OneToMany(mappedBy = "comment", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<Comment> comments;
}
