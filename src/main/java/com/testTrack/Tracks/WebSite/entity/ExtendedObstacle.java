package com.testTrack.Tracks.WebSite.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ExtendedObstacle {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private double lengthObstacle;
    private String obstaclePavement;
    private String roughTerrain;
    private String type;

    @ManyToOne(fetch = FetchType.EAGER)
    private Coordinate startCoordinate;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Track track;

}
