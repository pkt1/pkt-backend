package com.testTrack.Tracks.WebSite.repository;

import com.testTrack.Tracks.WebSite.entity.Track;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrackRepo extends CrudRepository<Track, Long> {
    Track getTrackById(Long id);
    List<Track> getAllByNearestTown(String nearestTown);
    List<Track> findAll();
    List<Track> findAllByName(String name);
}
