package com.testTrack.Tracks.WebSite.repository;

import com.testTrack.Tracks.WebSite.entity.Comment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepo extends CrudRepository<Comment, Long> {
    Comment getCommentById(Long id);
}
