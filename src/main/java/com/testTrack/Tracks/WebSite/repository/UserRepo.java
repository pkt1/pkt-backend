package com.testTrack.Tracks.WebSite.repository;

import com.testTrack.Tracks.WebSite.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo extends CrudRepository<User, Long> {
    User getUserById(Long id);
    User getUserByLogin(String login);
    User getUserByLoginAndPassword(String login, String password);
}
