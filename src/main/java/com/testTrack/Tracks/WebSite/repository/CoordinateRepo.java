package com.testTrack.Tracks.WebSite.repository;

import com.testTrack.Tracks.WebSite.entity.Coordinate;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CoordinateRepo extends CrudRepository<Coordinate, Long> {
    Coordinate findCoordinateByHeightAndXAndY(double height, double x, double y);
    Coordinate findCoordinateById (Long id);
    List<Coordinate> findAll();
}
