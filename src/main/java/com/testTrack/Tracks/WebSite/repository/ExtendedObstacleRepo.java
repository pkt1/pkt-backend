package com.testTrack.Tracks.WebSite.repository;

import com.testTrack.Tracks.WebSite.entity.ExtendedObstacle;
import com.testTrack.Tracks.WebSite.entity.LocalObstacle;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface ExtendedObstacleRepo extends CrudRepository<ExtendedObstacle, Long> {
    ExtendedObstacle findExtendedObstacleById(Long id);
    ArrayList<ExtendedObstacle> findAll();
}
