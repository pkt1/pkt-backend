package com.testTrack.Tracks.WebSite.repository;

import com.testTrack.Tracks.WebSite.entity.LocalObstacle;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface LocalObstacleRepo extends CrudRepository<LocalObstacle, Long> {
    LocalObstacle findLocalObstacleById(Long id);
    ArrayList<LocalObstacle> findAll();
}
