package com.testTrack.Tracks.WebSite.sort;

import com.testTrack.Tracks.WebSite.entity.Track;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class TrackSort {
    public List<Track> sort(List<Track> tracks, String sortType){
        switch (sortType) {
            case "bytime":
                return sortByTime(tracks);
            case "byscore":
                return sortByCategory(tracks);
            case "bylength":
                return sortByLength(tracks);
        }
        return tracks;
    }
    private List<Track> sortByTime(List<Track> tracks){
        tracks.sort((o1, o2) -> {
            DateFormat formatForDateNow = new SimpleDateFormat("E yyyy.MM.dd 'и время' hh:mm:ss a zzz");
            Date date1 = null;
            Date date2 = null;
            try {
                date1 = formatForDateNow.parse(o2.getCreationDate());
                date2 = formatForDateNow.parse(o1.getCreationDate());
                return date1.compareTo(date2);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return 0;
        });
        return tracks;
    }

    private List<Track> sortByCategory(List<Track> tracks){
        tracks.sort(Comparator.comparingInt(Track::getCategory));
        return tracks;
    }

    private List<Track> sortByLength(List<Track> tracks){
        tracks.sort(Comparator.comparingDouble(Track::getLength));
        return tracks;
    }
}
