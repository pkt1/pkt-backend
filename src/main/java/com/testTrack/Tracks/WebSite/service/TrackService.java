package com.testTrack.Tracks.WebSite.service;

import com.testTrack.Tracks.WebSite.entity.Track;
import com.testTrack.Tracks.WebSite.entity.User;
import com.testTrack.Tracks.WebSite.repository.TrackRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TrackService {
    private final TrackRepo trackRepo;

    public TrackService(TrackRepo trackRepo) {
        this.trackRepo = trackRepo;
    }


    public void create(Track track){
        trackRepo.save(track);
    }

    public Track read(Long id){
        return trackRepo.getTrackById(id);
    }

    public boolean update(Track track, Long id){
        Track dbTrack = trackRepo.getTrackById(id);
        if(dbTrack != null){
            track.setId(id);
            trackRepo.save(track);
            return true;
        }
        return false;
    }

    public void delete(Long id){
        trackRepo.deleteById(id);
    }

    public List<Track> readAllByCityName(String cityName){
        return trackRepo.getAllByNearestTown(cityName);
    }
    public List<Track> readAll(){
        return trackRepo.findAll();
    }
    public  List<Track> readAllByName(String name){
        return trackRepo.findAllByName(name);
    }

}
