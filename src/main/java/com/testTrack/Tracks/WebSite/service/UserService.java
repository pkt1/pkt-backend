package com.testTrack.Tracks.WebSite.service;

import com.testTrack.Tracks.WebSite.entity.Comment;
import com.testTrack.Tracks.WebSite.entity.User;
import com.testTrack.Tracks.WebSite.repository.UserRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserRepo userRepo;

    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public void create(User user){
        userRepo.save(user);
    }

    public User read(Long id){
        return userRepo.getUserById(id);
    }

    public boolean update(User user, Long id){
        User dbUser = userRepo.getUserById(id);
        if(dbUser != null){
            user.setId(id);
            userRepo.save(user);
            return true;
        }
        return false;
    }

    public void delete(Long id){
        userRepo.deleteById(id);
    }


    public User getUserByLogin(String login){
        return userRepo.getUserByLogin(login);
    }
    public User getUserByLoginAndPassword(String login, String password){
        return userRepo.getUserByLoginAndPassword(login, password);
    }
}
