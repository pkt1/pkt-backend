package com.testTrack.Tracks.WebSite.service;

import com.testTrack.Tracks.WebSite.entity.ExtendedObstacle;
import com.testTrack.Tracks.WebSite.entity.LocalObstacle;
import com.testTrack.Tracks.WebSite.repository.ExtendedObstacleRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExtendedObstacleService{
    private final ExtendedObstacleRepo extendedObstacleRepo;

    public ExtendedObstacleService(ExtendedObstacleRepo extendedObstacleRepo) {
        this.extendedObstacleRepo = extendedObstacleRepo;
    }

    public void create(ExtendedObstacle extendedObstacle){
        extendedObstacleRepo.save(extendedObstacle);
    }

    public ExtendedObstacle read(Long id){
        return extendedObstacleRepo.findExtendedObstacleById(id);
    }


    public boolean update(ExtendedObstacle extendedObstacle, Long id){
        ExtendedObstacle dbExtendedObstacle = extendedObstacleRepo.findExtendedObstacleById(id);
        if(dbExtendedObstacle != null){
            extendedObstacle.setId(id);
            extendedObstacleRepo.save(extendedObstacle);
            return true;
        }
        return false;
    }

    public void delete(Long id){
        extendedObstacleRepo.deleteById(id);
    }

    public List<ExtendedObstacle> readAll(){
        return extendedObstacleRepo.findAll();
    }
}
