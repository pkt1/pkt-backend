package com.testTrack.Tracks.WebSite.service;

import com.testTrack.Tracks.WebSite.entity.Coordinate;
import com.testTrack.Tracks.WebSite.entity.Track;
import com.testTrack.Tracks.WebSite.repository.CoordinateRepo;
import com.testTrack.Tracks.WebSite.repository.TrackRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CoordinateService {
    private final CoordinateRepo coordinateRepo;

    public CoordinateService(CoordinateRepo coordinateRepo) {
        this.coordinateRepo = coordinateRepo;
    }


    public void create(Coordinate coordinate){
        coordinateRepo.save(coordinate);
    }

    public Coordinate read(Long id){
        return coordinateRepo.findCoordinateById(id);
    }

    public Coordinate readByXAndYAndHeight(double x, double y, double height){
        return coordinateRepo.findCoordinateByHeightAndXAndY(height, x, y);
    }

    public boolean update(Coordinate coordinate, Long id){
        Coordinate dbCoordinate = coordinateRepo.findCoordinateById(id);
        if(dbCoordinate != null){
            coordinate.setId(id);
            coordinateRepo.save(coordinate);
            return true;
        }
        return false;
    }

    public void delete(Long id){
        coordinateRepo.deleteById(id);
    }

    public List<Coordinate> readAll(){
        return coordinateRepo.findAll();
    }
}
