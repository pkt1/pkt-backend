package com.testTrack.Tracks.WebSite.service;

import com.testTrack.Tracks.WebSite.entity.Coordinate;
import com.testTrack.Tracks.WebSite.entity.LocalObstacle;
import com.testTrack.Tracks.WebSite.repository.CoordinateRepo;
import com.testTrack.Tracks.WebSite.repository.LocalObstacleRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocalObstacleService {
    private final LocalObstacleRepo localObstacleRepo;

    public LocalObstacleService(LocalObstacleRepo localObstacleRepo) {
        this.localObstacleRepo = localObstacleRepo;
    }


    public void create(LocalObstacle localObstacle){
        localObstacleRepo.save(localObstacle);
    }

    public LocalObstacle read(Long id){
        return localObstacleRepo.findLocalObstacleById(id);
    }


    public boolean update(LocalObstacle localObstacle, Long id){
        LocalObstacle dbLocalObstacle = localObstacleRepo.findLocalObstacleById(id);
        if(dbLocalObstacle != null){
            localObstacle.setId(id);
            localObstacleRepo.save(localObstacle);
            return true;
        }
        return false;
    }

    public void delete(Long id){
        localObstacleRepo.deleteById(id);
    }

    public List<LocalObstacle> readAll(){
        return localObstacleRepo.findAll();
    }
}
