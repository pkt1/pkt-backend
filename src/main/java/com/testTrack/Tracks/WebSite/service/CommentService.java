package com.testTrack.Tracks.WebSite.service;

import com.testTrack.Tracks.WebSite.entity.Comment;
import com.testTrack.Tracks.WebSite.repository.CommentRepo;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    private final CommentRepo commentRepo;

    public CommentService(CommentRepo commentRepo) {
        this.commentRepo = commentRepo;
    }

    public void create(Comment comment){
        commentRepo.save(comment);
    }

    public Comment read(Long id){
        return commentRepo.getCommentById(id);
    }

    public boolean update(Comment comment, Long id){
        Comment dbComment = commentRepo.getCommentById(id);
        if(dbComment != null){
            comment.setId(id);
            commentRepo.save(comment);
            return true;
        }
        return false;
    }

    public void delete(Long id){
        commentRepo.deleteById(id);
    }
}
