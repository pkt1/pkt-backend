package com.testTrack.Tracks.WebSite.calculateCategory;

import com.testTrack.Tracks.WebSite.entity.Coordinate;
import com.testTrack.Tracks.WebSite.entity.ExtendedObstacle;

import java.util.ArrayList;

public class ExtendedObstacleCalculator {
    public double getScore(ExtendedObstacle extendedObstacle){
        ArrayList<Coordinate> coordinates = getCoordinatesForObstacle(extendedObstacle.getStartCoordinate(),
                extendedObstacle.getLengthObstacle(),
                new ArrayList<>(extendedObstacle.getTrack().getCoordinates()));
        if(coordinates == null){
            return 0;
        }
        return switch (extendedObstacle.getType()){
            case "Горный участок" ->
                    getCoefficientOfClimb(extendedObstacle.getLengthObstacle(), coordinates) *
                            getCoefficientOfHeight(coordinates) *
                            getCoefficientOfPavement(extendedObstacle.getType(), extendedObstacle.getObstaclePavement()) *
                            getCoefficientOfAngle(extendedObstacle.getLengthObstacle(), coordinates);

            case "Равниный участок" ->
                    getCoefficientOfPavement(extendedObstacle.getType(), extendedObstacle.getObstaclePavement()) *
                    getCoefficientOfLength(extendedObstacle.getLengthObstacle()) *
                    getCoefficientOfRoughTerrain(extendedObstacle.getRoughTerrain()) *
                    getCoefficientOfHeight(coordinates);
            default -> 0;
        };

    }

    private double getCoefficientOfAngle(double lengthObstacle, ArrayList<Coordinate> coordinates) {
        double angle = ((coordinates.get(coordinates.size() - 1).getHeight() -
                coordinates.get(0).getHeight()) / lengthObstacle) * 100;
        if(angle < 4){
            return 1;
        } else if(angle <= 6){
            return interpolation(4,6,1,1.1, angle);
        } else if(angle <= 9){
            return interpolation(6,9,1.1,1.3, angle);
        } else if(angle <= 12){
            return interpolation(9,12,1.3,1.5, angle);
        } else if(angle <= 14){
            return interpolation(12,14,1.5,1.8, angle);
        } else if(angle <= 15){
            return interpolation(14,15,1.8,2, angle);
        }
        return 2;
    }

    private double getCoefficientOfClimb(double lengthObstacle, ArrayList<Coordinate> coordinates) {
        double climb = 0;
        for(int i = 1; i < coordinates.size(); i++){
            if((coordinates.get(i).getHeight() - coordinates.get(i - 1).getHeight()) > 0){
                climb += coordinates.get(i).getHeight() - coordinates.get(i - 1).getHeight();
            }
        }
        if(climb < 200){
            return 1;
        } else if(climb < 500){
            return 1.1;
        } else if(climb < 800){
            return 1.2;
        } else if(climb < 1100){
            return 1.4;
        } else if(climb < 1300){
            return 1.6;
        } else if(climb < 1500){
            return 1.8;
        }
        return 2.1;
    }

    private double getCoefficientOfHeight(ArrayList<Coordinate> coordinates) {
        double climb = 0;
        for (Coordinate coordinate : coordinates) {
            climb += coordinate.getHeight();
        }
        double averageHeight = climb / coordinates.size();
        if(averageHeight < 500){
            return 1;
        } else if(averageHeight == 500){
            return 1;
        } else if(averageHeight < 1000){
            return interpolation(500, 1000, 1, 1.1, averageHeight);
        } else if(averageHeight < 1500){
            return interpolation(1000, 1500, 1.1, 1.2, averageHeight);
        } else if(averageHeight < 2000){
            return interpolation(1500, 2000, 1.2, 1.3, averageHeight);
        } else if(averageHeight < 2500){
            return interpolation(2000, 2500, 1.3, 1.4, averageHeight);
        } else if(averageHeight < 3000){
            return interpolation(2500, 3000, 1.4, 1.8, averageHeight);
        }
        return 1.8;
    }

    private double getCoefficientOfRoughTerrain(String roughTerrain) {
        return switch (roughTerrain){
            case "Плоская равнина с перепадами высот не более 30 м" -> 0.8;
            case "Слабопересеченная местность со средними уклонами подъемов 4% и перепадами высот от 30 до 50 м." -> 1.0;
            case "Среднепересеченная местность со средними уклонами подъемов 8% и перепадами высот от 50 до 100 м" -> 1.2;
            case "Сильнопересеченная местность с уклонами дорог  9% и более и перепадами высот от 100 до 200 м" -> 1.4;
            default -> 0;
        };
    }

    private double getCoefficientOfLength(double lengthObstacle) {
        double coefficient = 1 + lengthObstacle/100;
        return Math.min(coefficient, 1.8);
    }

    private double getCoefficientOfPavement(String type, String obstaclePavement) {
        switch (obstaclePavement){
            case "Асфальт":
                if(type.equals("Горная местность")){
                    return 0.9;
                }
                return 0.7;
            case "Профилированная гравийная (гравий до 10 мм) или грунтовая дорога (грейдер)":
                if(type.equals("Горная местность")){
                    return 1.0;
                }
                return 0.8;
            case "Непрофилированная грунтовая дорога (проселок), профилированная горная дорога, " +
                    "мелкощебеночная дорога (преобладающий размер камней 10–20 мм)":
                if(type.equals("Горная местность")){
                    return 1.2;
                }
                return 1.0;
            case "Каменистая горная дорога со значительными неровностями, уступами, выступающими частями скального " +
                    "массива, крупнощебеночная дорога (преобладающий размер камней 30 мм и выше), песчаная дорога " +
                    "(песок глубиной 3–5 см)":
                return 1.4;
            case "Используемая лесовозная, тракторная дорога, песчаная дорога (песок глубиной 5–10 см)," +
                    " скотопрогонная (конная) троп":
                return 1.8;
            case "Зимник летом, заброшенная лесовозная, тракторная дорога, песчаная дорога " +
                    "(песок более 10 см), туристская тропа":
                return 2.4;
            case "Труднопроходимое бездорожье, осложненное множеством мелких локальных препятствий":
                return 3.0;
            default:
                return 0;
        }
    }

    private ArrayList<Coordinate> getCoordinatesForObstacle(Coordinate startCoordinate, double lengthObstacle,
                                                            ArrayList<Coordinate> coordinates){
        ArrayList<Coordinate> answer = new ArrayList<>();
        answer.add(startCoordinate);

        int index = getIndex(coordinates, startCoordinate);
        if(index == 0){
            return null;
        }
        double tempLength = lengthObstacle;

        while (tempLength > 0){
            Coordinate coordinate1 = coordinates.get(index - 1);
            Coordinate coordinate2 = coordinates.get(index);
            tempLength -= Math.sqrt(Math.pow(coordinate2.getX() - coordinate1.getX(), 2) +
                    Math.pow(coordinate2.getY() - coordinate1.getY(), 2) +
                    Math.pow(coordinate2.getHeight() - coordinate1.getHeight(), 2));
            answer.add(coordinate2);
        }
        return answer;
    }

    private int getIndex(ArrayList<Coordinate> coordinates, Coordinate startCoordinate){
        for(int i = 1; i < coordinates.size(); i++){
            Coordinate coordinate1 = coordinates.get(i -1);
            Coordinate coordinate2 = coordinates.get(i);
            if((((coordinate1.getX() - startCoordinate.getX()) > 0 &&
                    (coordinate2.getX() - startCoordinate.getX()) < 0) ||
                    (((coordinate1.getX() - startCoordinate.getX()) < 0 &&
                            (coordinate2.getX() - startCoordinate.getX()) > 0))) &&
                    (((coordinate1.getY() - startCoordinate.getY()) > 0 &&
                            (coordinate2.getY() - startCoordinate.getY()) < 0) ||
                            (((coordinate1.getY() - startCoordinate.getY()) < 0 &&
                                    (coordinate2.getY() - startCoordinate.getY()) > 0))) ||
                    ((coordinate1.getX() - startCoordinate.getX()) == 0 ||
                            (coordinate2.getX() - startCoordinate.getX()) == 0)){
                return i;
            }
        }
        return 0;
    }

    private double interpolation(double x1, double x2, double y1, double y2,
                                 double xForInterpolation){
        return y1 + (((x2 - x1)/(y2 - y1)) * (xForInterpolation - x1));
    }
}
