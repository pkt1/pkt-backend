package com.testTrack.Tracks.WebSite.calculateCategory;

import com.testTrack.Tracks.WebSite.entity.Coordinate;
import com.testTrack.Tracks.WebSite.entity.ExtendedObstacle;
import com.testTrack.Tracks.WebSite.entity.LocalObstacle;

import java.util.Collection;

public class ObstacleCalculator {
    private final ExtendedObstacleCalculator extendedObstacleCalculator;
    private final LocalObstacleCalculator localObstacleCalculator;

    public ObstacleCalculator() {
        this.extendedObstacleCalculator = new ExtendedObstacleCalculator();
        this.localObstacleCalculator = new LocalObstacleCalculator();
    }

    public double getEquivalentMileageFromLocalObstacle(LocalObstacle localObstacle) {
        return localObstacleCalculator.getEquivalentMileage(localObstacle);
    }

    public double getScoreFromExtendedObstacle(ExtendedObstacle extendedObstacle) {
        return extendedObstacleCalculator.getScore(extendedObstacle);
    }
}

