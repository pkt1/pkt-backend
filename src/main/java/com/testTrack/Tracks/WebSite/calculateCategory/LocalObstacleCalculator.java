package com.testTrack.Tracks.WebSite.calculateCategory;

import com.testTrack.Tracks.WebSite.entity.LocalObstacle;

public class LocalObstacleCalculator {
    public double getEquivalentMileage(LocalObstacle localObstacle) {
        switch (localObstacle.getType()) {
            case "Переправы":
                return switch (localObstacle.getCategory()) {
                    case "н/к" -> 2;
                    case "1А" -> 10;
                    case "1Б" -> 20;
                    case "2А" -> 40;
                    case "2Б" -> 80;
                    default -> 0;
                };
            case "Перевальные взлеты без тропы (за каждые 100 м пути)":
                if (localObstacle.getLength() > 100) {
                    return switch (localObstacle.getCategory()) {
                        case "н/к" -> 2 * (int) (localObstacle.getLength() / 100);
                        case "1А" -> 10 * (int) (localObstacle.getLength() / 100);
                        default -> 0;
                    };
                }
                return 0;
            case "Каньоны (за каждые 100 м пути)":
                if (localObstacle.getLength() > 200) {
                    return switch (localObstacle.getCategory()) {
                        case "н/к" -> (int) (localObstacle.getLength() / 100);
                        case "1А" -> 2 * (int) (localObstacle.getLength() / 100);
                        case "1Б" -> 4 * (int) (localObstacle.getLength() / 100);
                        default -> 0;
                    };
                }
                break;
            case "Болота (за каждые 100 м пути)":
                if (localObstacle.getLength() > 100) {
                    return switch (localObstacle.getCategory()) {
                        case "н/к" -> 2 * (int) (localObstacle.getLength() / 100);
                        case "1А" -> 4 * (int) (localObstacle.getLength() / 100);
                        case "1Б" -> 8 * (int) (localObstacle.getLength() / 100);
                        default -> 0;
                    };
                }
                break;
            case "Осыпи, морены (за каждые 100 м пути)":
            case "Снежно-ледовые участки (за каждые 100 м пути)":
                if (localObstacle.getLength() > 100) {
                    return switch (localObstacle.getCategory()) {
                        case "н/к" -> 2 * (int) (localObstacle.getLength() / 100);
                        case "1А" -> 4 * (int) (localObstacle.getLength() / 100);
                        default -> 0;
                    };
                }
                break;
            case "Пески (за каждые 100 м пути)":
                if (localObstacle.getLength() > 100) {
                    return switch (localObstacle.getCategory()) {
                        case "н/к" -> 0.5 * (int) (localObstacle.getLength() / 100);
                        case "1А" -> 1.5 * (int) (localObstacle.getLength() / 100);
                        default -> 0;
                    };
                }
                break;
            case "Растительный покров (за каждые 100 м пути)":
                if (localObstacle.getLength() > 100) {
                    return switch (localObstacle.getCategory()) {
                        case "н/к" -> 0.5 * (int) (localObstacle.getLength() / 100);
                        case "1А" -> (int) (localObstacle.getLength() / 100);
                        case "1Б" -> 2 * (int) (localObstacle.getLength() / 100);
                        case "2А" -> 4 * (int) (localObstacle.getLength() / 100);
                        case "2Б" -> 8 * (int) (localObstacle.getLength() / 100);
                        default -> 0;
                    };
                }
                break;
        }
        return 0;
    }
}
