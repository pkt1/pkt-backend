package com.testTrack.Tracks.WebSite.calculateCategory;

import com.testTrack.Tracks.WebSite.entity.ExtendedObstacle;
import com.testTrack.Tracks.WebSite.entity.LocalObstacle;
import com.testTrack.Tracks.WebSite.entity.Track;

import java.util.Collection;

public class CalculateCategory {
    private final ObstacleCalculator obstacleCalculator;

    public CalculateCategory() {
        this.obstacleCalculator = new ObstacleCalculator();
    }

    public int getCategory(Track track){
        int scoreFromExtendedObstacles = 0;
        for(ExtendedObstacle extendedObstacle : track.getExtendedObstacles()){
            scoreFromExtendedObstacles += obstacleCalculator.getScoreFromExtendedObstacle(extendedObstacle);
        }
        if(getScoreByTrack(scoreFromExtendedObstacles, track, 6) >= 60){
            return 6;
        } else if(getScoreByTrack(scoreFromExtendedObstacles, track, 5) >= 40){
            return 5;
        } else if(getScoreByTrack(scoreFromExtendedObstacles, track, 4) >= 25){
            return 4;
        } else if(getScoreByTrack(scoreFromExtendedObstacles, track, 3) >= 15){
            return 3;
        } else if(getScoreByTrack(scoreFromExtendedObstacles, track, 2) >= 7){
            return 2;
        } else if(getScoreByTrack(scoreFromExtendedObstacles, track, 1) >= 2){
            return 1;
        }
        return 0;
    }

    private double getIntensive(Track track, int category){
        double equivalentMileage = 0;
        for(LocalObstacle localObstacle : track.getLocalObstacles()){
            equivalentMileage += obstacleCalculator.getEquivalentMileageFromLocalObstacle(localObstacle);
        }
        return switch (category){
            case 1 -> (track.getLength() + equivalentMileage) * 6 * 1.2 / (track.getNumberDays() * 300);
            case 2 -> (track.getLength() + equivalentMileage) * 8 * 1.2 / (track.getNumberDays() * 400);
            case 3 -> (track.getLength() + equivalentMileage) * 10 * 1.2 / (track.getNumberDays() * 500);
            case 4 -> (track.getLength() + equivalentMileage) * 13 * 1.2 / (track.getNumberDays() * 600);
            case 5 -> (track.getLength() + equivalentMileage) * 16 * 1.2 / (track.getNumberDays() * 700);
            case 6 -> (track.getLength() + equivalentMileage) * 20 * 1.2 / (track.getNumberDays() * 800);
            default -> 0;
        };
    }

    private double getScoreByTrack(double scoreFromExtendedObstacles, Track track, int category){
        return scoreFromExtendedObstacles * getIntensive(track, category) + 10;
    }
}
