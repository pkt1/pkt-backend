package com.testTrack.Tracks.WebSite.GPX;

import com.testTrack.Tracks.WebSite.entity.Coordinate;
import io.jenetics.jpx.GPX;
import io.jenetics.jpx.Track;
import io.jenetics.jpx.TrackSegment;
import io.jenetics.jpx.WayPoint;
import io.jenetics.jpx.geom.Geoid;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class GPXReader {
    public ArrayList<Coordinate> getCoordinatesFromGPX(MultipartFile file) throws IOException {
        ArrayList<Coordinate> coordinates = new ArrayList<>();
        GPX.read(file.getInputStream()).tracks().
                flatMap(Track::segments)
                .flatMap(TrackSegment::points)
                .forEach(x-> coordinates.add(x.getElevation().isPresent()
                        ? new Coordinate(x.getLatitude().doubleValue(),
                            x.getLongitude().doubleValue(),
                            x.getElevation().get().doubleValue())
                        : new Coordinate(x.getLatitude().doubleValue(),
                            x.getLongitude().doubleValue(), 0)));
        return coordinates;
    }
    public int getDaysFromGPX(MultipartFile file) throws IOException {
        GPX gpx = GPX.read(file.getInputStream());
        Optional<ZonedDateTime> timeStart = gpx.getTracks().get(0).getSegments().get(0).getPoints().get(0).getTime();
        Optional<ZonedDateTime> timeEnd = gpx.getTracks().get(gpx.getTracks().size() - 1).getSegments().
                get(gpx.getTracks().get(gpx.getTracks().size() - 1).getSegments().size() - 1).getPoints().
                get(gpx.getTracks().get(gpx.getTracks().size() - 1).getSegments().
                        get(gpx.getTracks().get(gpx.getTracks().size() - 1).
                                getSegments().size() - 1).getPoints().size() - 1).getTime();
        if(timeStart.isPresent() && timeEnd.isPresent()) {
            return Math.abs((int) ChronoUnit.DAYS.between(timeEnd.get(), timeStart.get()));
        } else {
            return 0;
        }
    }
}
