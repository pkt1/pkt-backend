package com.testTrack.Tracks.WebSite.controller;

import com.testTrack.Tracks.WebSite.Requests.ExtendedObstacleCreateRequest;
import com.testTrack.Tracks.WebSite.entity.ExtendedObstacle;
import com.testTrack.Tracks.WebSite.entity.LocalObstacle;
import com.testTrack.Tracks.WebSite.entity.Track;
import com.testTrack.Tracks.WebSite.service.ExtendedObstacleService;
import com.testTrack.Tracks.WebSite.service.TrackService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("extendedobstacle")
public class ExtendedObstacleController {
    private final ExtendedObstacleService extendedObstacleService;
    private final TrackService trackService;

    public ExtendedObstacleController(ExtendedObstacleService extendedObstacleService, TrackService trackService) {
        this.extendedObstacleService = extendedObstacleService;
        this.trackService = trackService;
    }

    @PutMapping("/")
    public ResponseEntity<?> create(@RequestBody ExtendedObstacleCreateRequest extendedObstacleCreateRequest){
        if(extendedObstacleCreateRequest.getExtendedObstacle().getRoughTerrain() == null ||
                extendedObstacleCreateRequest.getExtendedObstacle().getObstaclePavement() == null ||
                extendedObstacleCreateRequest.getExtendedObstacle().getType() == null ||
                extendedObstacleCreateRequest.getExtendedObstacle().getStartCoordinate() == null ||
                extendedObstacleCreateRequest.getExtendedObstacle().getLengthObstacle() == 0){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Track track = trackService.read(extendedObstacleCreateRequest.getId());
        if(track != null) {
            extendedObstacleCreateRequest.getExtendedObstacle().setTrack(track);
            extendedObstacleService.create(extendedObstacleCreateRequest.getExtendedObstacle());
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        extendedObstacleService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable Long id){
        final ExtendedObstacle extendedObstacle = extendedObstacleService.read(id);
        return extendedObstacle != null
                ? new ResponseEntity<>(extendedObstacle, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody ExtendedObstacle extendedObstacle){
        final boolean updated = extendedObstacleService.update(extendedObstacle, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

}
