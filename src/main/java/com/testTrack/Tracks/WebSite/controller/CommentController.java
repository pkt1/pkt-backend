package com.testTrack.Tracks.WebSite.controller;

import com.testTrack.Tracks.WebSite.Requests.CommentCreateRequest;
import com.testTrack.Tracks.WebSite.autfConfig.jwt.JwtTokenService;
import com.testTrack.Tracks.WebSite.entity.Comment;
import com.testTrack.Tracks.WebSite.entity.Track;
import com.testTrack.Tracks.WebSite.entity.User;
import com.testTrack.Tracks.WebSite.service.CommentService;
import com.testTrack.Tracks.WebSite.service.TrackService;
import com.testTrack.Tracks.WebSite.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("comment")
public class CommentController {
    private final CommentService commentService;
    private final TrackService trackService;
    private final UserService userService;
    private final JwtTokenService jwtTokenService;

    public CommentController(CommentService commentService, TrackService trackService, UserService userService, JwtTokenService jwtTokenService) {
        this.commentService = commentService;
        this.trackService = trackService;
        this.userService = userService;
        this.jwtTokenService = jwtTokenService;
    }

    @PutMapping("/")
    public ResponseEntity<?> create(@RequestBody CommentCreateRequest commentCreateRequest, @RequestHeader("Authorization") String token){
        if(commentCreateRequest.getComment().getText() == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        User user = userService.getUserByLogin(jwtTokenService.getUsername(jwtTokenService.resolveToken(token)));
        Track track = trackService.read(commentCreateRequest.getId());
        if(track != null) {
            commentCreateRequest.getComment().setTrack(track);
            commentCreateRequest.getComment().setAuthor(user);
            commentService.create(commentCreateRequest.getComment());
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        commentService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable Long id){
        final Comment comment = commentService.read(id);
        return comment != null
                ? new ResponseEntity<>(comment, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Comment comment){
        final boolean updated = commentService.update(comment, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }
}
