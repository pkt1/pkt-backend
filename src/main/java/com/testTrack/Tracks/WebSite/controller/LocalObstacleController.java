package com.testTrack.Tracks.WebSite.controller;

import com.testTrack.Tracks.WebSite.Requests.LocalObstacleCreateRequest;
import com.testTrack.Tracks.WebSite.entity.Comment;
import com.testTrack.Tracks.WebSite.entity.LocalObstacle;
import com.testTrack.Tracks.WebSite.entity.Track;
import com.testTrack.Tracks.WebSite.service.LocalObstacleService;
import com.testTrack.Tracks.WebSite.service.TrackService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("localobstacle")
public class LocalObstacleController {
    public final LocalObstacleService localObstacleService;
    public final TrackService trackService;

    public LocalObstacleController(LocalObstacleService localObstacleService, TrackService trackService) {
        this.localObstacleService = localObstacleService;
        this.trackService = trackService;
    }

    @PutMapping("/")
    public ResponseEntity<?> create(@RequestBody LocalObstacleCreateRequest localObstacleCreateRequest){
        if(localObstacleCreateRequest.getLocalObstacle().getStartCoordinate() == null ||
                localObstacleCreateRequest.getLocalObstacle().getLength() == 0 ||
                localObstacleCreateRequest.getLocalObstacle().getType() == null ||
                localObstacleCreateRequest.getLocalObstacle().getCategory() == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Track track = trackService.read(localObstacleCreateRequest.getId());
        if(track != null) {
            localObstacleCreateRequest.getLocalObstacle().setTrack(track);
            localObstacleService.create(localObstacleCreateRequest.getLocalObstacle());
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        localObstacleService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable Long id){
        final LocalObstacle localObstacle = localObstacleService.read(id);
        return localObstacle != null
                ? new ResponseEntity<>(localObstacle, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody LocalObstacle localObstacle){
        final boolean updated = localObstacleService.update(localObstacle, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }
}
