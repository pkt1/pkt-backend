package com.testTrack.Tracks.WebSite.controller;

import com.testTrack.Tracks.WebSite.autfConfig.jwt.JwtTokenService;
import com.testTrack.Tracks.WebSite.Requests.AuthRequest;
import com.testTrack.Tracks.WebSite.entity.User;
import com.testTrack.Tracks.WebSite.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class AuthController {
    private final AuthenticationManager authenticationManager;

    private final JwtTokenService jwtTokenService;

    private final UserService userService;

    public AuthController(AuthenticationManager authenticationManager, JwtTokenService jwtTokenService,
                                          UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenService = jwtTokenService;
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AuthRequest request) {
        try {
            String login = request.getUsername();
            User user = userService.getUserByLoginAndPassword(login, request.getPassword());

            if (user == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            String token = jwtTokenService.createToken(login, user.getPassword());

            Map<Object, Object> response = new HashMap<>();
            response.put("login", login);
            response.put("token", token);

            return new ResponseEntity<>(response ,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}

