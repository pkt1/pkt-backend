package com.testTrack.Tracks.WebSite.controller;

import com.testTrack.Tracks.WebSite.GPX.GPXReader;
import com.testTrack.Tracks.WebSite.Requests.CategoryAnswer;
import com.testTrack.Tracks.WebSite.Requests.TrackCreateRequest;
import com.testTrack.Tracks.WebSite.autfConfig.jwt.JwtTokenService;
import com.testTrack.Tracks.WebSite.calculateCategory.CalculateCategory;
import com.testTrack.Tracks.WebSite.entity.*;
import com.testTrack.Tracks.WebSite.service.CoordinateService;
import com.testTrack.Tracks.WebSite.service.TrackService;
import com.testTrack.Tracks.WebSite.service.UserService;
import com.testTrack.Tracks.WebSite.sort.TrackSort;
import io.jenetics.jpx.Point;
import io.jenetics.jpx.WayPoint;
import io.jenetics.jpx.geom.Geoid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class TrackController {
    private final TrackService trackService;
    private final CoordinateService coordinateService;
    private final UserService userService;
    private final TrackSort trackSort;
    private final GPXReader gpxReader;
    private final CalculateCategory calculateCategory;
    private final JwtTokenService jwtTokenService;

    public TrackController(TrackService trackService, CoordinateService coordinateService, UserService userService, TrackSort trackSort, JwtTokenService jwtTokenService) {
        this.trackService = trackService;
        this.coordinateService = coordinateService;
        this.userService = userService;
        this.trackSort = trackSort;
        this.jwtTokenService = jwtTokenService;
        this.calculateCategory = new CalculateCategory();
        this.gpxReader = new GPXReader();
    }

    @GetMapping("track/{id}")
    private ResponseEntity<?> get(@PathVariable Long id){
        final Track track = trackService.read(id);
        return track != null
                ? new ResponseEntity<>(track, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("track/create/start")
    public ResponseEntity<?> startCreate(@RequestBody TrackCreateRequest trackCreateRequest, @RequestHeader("Authorization") String token){
        if(trackCreateRequest.getName() == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        User dbUser = userService.getUserByLogin(jwtTokenService.getUsername(jwtTokenService.resolveToken(token)));
        if(dbUser != null) {
            Track track = new Track();
            track.setOwner(dbUser);
            track.setName(trackCreateRequest.getName());
            track.setNearestTown(trackCreateRequest.getCityName());
            track.setDescription(trackCreateRequest.getDescription());
            SimpleDateFormat formatForDateNow = new SimpleDateFormat("E yyyy.MM.dd 'и время' hh:mm:ss a zzz");
            track.setCreationDate(formatForDateNow.format(new Date()));
            trackService.create(track);
            return new ResponseEntity<>(track, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("track/{id}/coordinates")
    public ResponseEntity<?> setCoordinates(@PathVariable Long id,@RequestParam MultipartFile file){
        try {
            if(file == null){
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            Track track = trackService.read(id);

            if(track != null) {
                ArrayList<Coordinate> coordinates = gpxReader.getCoordinatesFromGPX(file);

                for(Coordinate coordinate : coordinates){
                    Coordinate dbCoordinate = coordinateService.readByXAndYAndHeight(coordinate.getX(),
                            coordinate.getY(), coordinate.getHeight());
                    if(dbCoordinate == null){
                        coordinateService.create(coordinate);
                    } else{
                        coordinate.setId(dbCoordinate.getId());
                    }
                }

                track.setCoordinates(coordinates);
                track.setNumberDays(gpxReader.getDaysFromGPX(file));
                track.setLength(getLengthFromCoordinates(coordinates));
                trackService.update(track, track.getId());
                ArrayList<Coordinate> coordinatesAnswer = new ArrayList<>();
                for(int i = 0; i < coordinates.size(); i += coordinates.size()/100){
                    coordinatesAnswer.add(coordinates.get(i));
                }
                if((coordinates.size() - 1) % 20 != 0){
                    coordinatesAnswer.add(coordinates.get(coordinates.size() - 1));
                }
                return new ResponseEntity<>(coordinatesAnswer, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("track/{id}/category")
    public ResponseEntity<?> setCategory(@PathVariable Long id, @RequestParam int category){
        Track track = trackService.read(id);

        if(track != null) {
            track.setCategory(category);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("track/{id}/category/calculate")
    public ResponseEntity<?> calculateCategory(@PathVariable Long id){
        Track track = trackService.read(id);

        if(track != null){
            CategoryAnswer categoryAnswer = new CategoryAnswer();
            categoryAnswer.setCategory(calculateCategory.getCategory(track));
            return new ResponseEntity<>(categoryAnswer, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("track/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        trackService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("track/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Track track){
        final boolean updated = trackService.update(track, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @GetMapping("tracks/")
    public ResponseEntity<?> getAll(@RequestParam(defaultValue = "1") int page,
                                     @RequestParam(defaultValue = "10") int count,
                                     @RequestParam (defaultValue = "bytime") String nameSort){
        List<Track> tracksDB = trackService.readAll();
        if(tracksDB != null){
            List<Track> tracks = new ArrayList<>();
            for(Track track : tracksDB){
                if(track.getCoordinates() != null){
                    tracks.add(track);
                }
            }
            if((tracks.size() - (page - 1) * count) > 0) {
                if ((tracks.size() - (page - 1) * count) < count) {
                    List<Track> tracksAnswer = trackSort.sort(tracks.subList(page * count - 10,
                            tracks.size()), nameSort);
                    return new ResponseEntity<>(tracksAnswer, HttpStatus.OK);
                }
                List<Track> tracksAnswer = trackSort.sort(tracks.subList(page * count - 10,
                        tracks.size()), nameSort);
                return new ResponseEntity<>(tracksAnswer, HttpStatus.OK);
            }
            return new ResponseEntity<>(new ArrayList<Track>(),HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<Track>(),HttpStatus.OK);
    }

    @GetMapping("tracks/bycity/")
    public ResponseEntity<?> getAllByCity(@RequestParam(name = "cityname") String cityName,
                                           @RequestParam(defaultValue = "1") int page,
                                           @RequestParam(defaultValue = "10") int count,
                                           @RequestParam (defaultValue = "bytime") String nameSort){
        List<Track> tracksDB = trackService.readAllByCityName(cityName);
        if(tracksDB != null){
            List<Track> tracks = new ArrayList<>();
            for(Track track : tracksDB){
                if(track.getCoordinates() != null){
                    tracks.add(track);
                }
            }
            if((tracks.size() - (page - 1) * count) > 0) {
                if ((tracks.size() - (page - 1) * count) < count) {
                    List<Track> tracksAnswer = trackSort.sort(tracks.subList(page * count - 10,
                            tracks.size()), nameSort);
                    return new ResponseEntity<>(tracksAnswer, HttpStatus.OK);
                }
                List<Track> tracksAnswer = trackSort.sort(tracks.subList(page * count - 10,
                        tracks.size()), nameSort);
                return new ResponseEntity<>(tracksAnswer, HttpStatus.OK);
            }
            return new ResponseEntity<>(new ArrayList<Track>(),HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<Track>(),HttpStatus.OK);
    }

    @GetMapping("tracks/byname/")
    public ResponseEntity<?> getAllByName(@RequestParam(name = "cityname") String name,
                                          @RequestParam(defaultValue = "1") int page,
                                          @RequestParam(defaultValue = "10") int count,
                                          @RequestParam (defaultValue = "bytime") String nameSort){
        List<Track> tracksDB = trackService.readAllByName(name);
        if(tracksDB != null){
            List<Track> tracks = new ArrayList<>();
            for(Track track : tracksDB){
                if(track.getCoordinates() != null){
                    tracks.add(track);
                }
            }
            if((tracks.size() - (page - 1) * count) > 0) {
                if ((tracks.size() - (page - 1) * count) < count) {
                    List<Track> tracksAnswer = trackSort.sort(tracks.subList(page * count - 10,
                            tracks.size()), nameSort);
                    return new ResponseEntity<>(tracksAnswer, HttpStatus.OK);
                }
                List<Track> tracksAnswer = trackSort.sort(tracks.subList(page * count - 10,
                        tracks.size()), nameSort);
                return new ResponseEntity<>(tracksAnswer, HttpStatus.OK);
            }
            return new ResponseEntity<>(new ArrayList<Track>(),HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<Track>(),HttpStatus.OK);
    }

    @GetMapping("track/{id}/coordinates")
    public ResponseEntity<?> getCoordinates(@PathVariable Long id){
        final Track track = trackService.read(id);

        if(track != null) {
            ArrayList<Coordinate> coordinates = new ArrayList<>(track.getCoordinates());
            ArrayList<Coordinate> coordinatesAnswer = new ArrayList<>();
            for (int i = 0; i < coordinates.size(); i += 20) {
                coordinatesAnswer.add(coordinates.get(i));
            }
            if ((coordinates.size() - 1) % 20 != 0) {
                coordinatesAnswer.add(coordinates.get(coordinates.size() - 1));
            }

            return new ResponseEntity<>(coordinatesAnswer, HttpStatus.OK);
        }

        return new ResponseEntity<>(new ArrayList<Coordinate>(),HttpStatus.OK);
    }

    @GetMapping("track/{id}/localobstacle")
    public ResponseEntity<?> getLocalObstacle(@PathVariable Long id){
        final Track track = trackService.read(id);

        return track != null
                ? new ResponseEntity<>(track.getLocalObstacles(), HttpStatus.OK)
                : new ResponseEntity<>(new ArrayList<LocalObstacle>(),HttpStatus.OK);
    }

    @GetMapping("track/{id}/extendedobstacle")
    public ResponseEntity<?> getExtendedObstacle(@PathVariable Long id){
        final Track track = trackService.read(id);

        return track != null
                ? new ResponseEntity<>(track.getExtendedObstacles(), HttpStatus.OK)
                : new ResponseEntity<>(new ArrayList<ExtendedObstacle>(),HttpStatus.OK);
    }

    @GetMapping("track/{id}/comments")
    public ResponseEntity<?> getComments(@PathVariable Long id){
        final Track track = trackService.read(id);
        return track != null
                ? new ResponseEntity<>(track.getComments(), HttpStatus.OK)
                : new ResponseEntity<>(new ArrayList<Comment>(),HttpStatus.OK);
    }

    public double getLengthFromCoordinates(ArrayList<Coordinate> coordinates){

        double length = 0;
        for(int i = 1; i < coordinates.size(); i++){
            Coordinate coordinate1 = coordinates.get(i - 1);
            Coordinate coordinate2 = coordinates.get(i);

            final Point start = WayPoint.of(coordinate1.getX(), coordinate1.getY());
            final Point end = WayPoint.of(coordinate2.getX(), coordinate2.getY());

            length += Math.abs(Geoid.WGS84.distance(start, end).doubleValue());
        }
        return length/1000;
    }

}
