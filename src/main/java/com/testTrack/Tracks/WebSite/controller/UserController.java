package com.testTrack.Tracks.WebSite.controller;

import com.testTrack.Tracks.WebSite.autfConfig.jwt.JwtTokenService;
import com.testTrack.Tracks.WebSite.entity.Track;
import com.testTrack.Tracks.WebSite.entity.User;
import com.testTrack.Tracks.WebSite.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController{
    private final UserService userService;
    private final JwtTokenService jwtTokenService;

    public UserController(UserService userService, JwtTokenService jwtTokenService) {
        this.userService = userService;
        this.jwtTokenService = jwtTokenService;
    }

    @PutMapping("/create")
    public ResponseEntity<?> create(@RequestBody User user){
        if(user.getLogin() == null || user.getPassword() == null ||
                userService.getUserByLogin(user.getLogin()) != null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        userService.create(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/")
    public ResponseEntity<?> delete(@RequestHeader("Authorization") String token){
        userService.delete(userService.getUserByLogin(jwtTokenService.getUsername(jwtTokenService.resolveToken(token))).getId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<?> get(@RequestHeader("Authorization") String token){
        final User user = userService.read(userService.getUserByLogin(jwtTokenService.getUsername(jwtTokenService.resolveToken(token))).getId());
        return user != null
                ? new ResponseEntity<>(user, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/")
    public ResponseEntity<?> update(@RequestHeader("Authorization") String token, @RequestBody User user){
        final boolean updated = userService.update(user,
                userService.getUserByLogin(jwtTokenService.getUsername(jwtTokenService.resolveToken(token))).getId());

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @PutMapping("/chosentrack/")
    public ResponseEntity<?> addToChosenTrack(@RequestHeader("Authorization") String token, @RequestParam Track track){
        User user = userService.getUserByLogin(jwtTokenService.getUsername(jwtTokenService.resolveToken(token)));
        if(user != null){
            user.getChosenTrack().add(track);
            if(userService.update(user, userService.getUserByLogin(jwtTokenService.getUsername(jwtTokenService.resolveToken(token))).getId())) {
                return new ResponseEntity<>(HttpStatus.OK);
            }
            new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/chosentrack/")
    public ResponseEntity<?> deleteChosenTrack(@RequestHeader("Authorization") String token, @RequestParam Track track){
        User user = userService.getUserByLogin(jwtTokenService.getUsername(jwtTokenService.resolveToken(token)));
        if(user != null){
            user.getChosenTrack().remove(track);
            if(userService.update(user, userService.getUserByLogin(jwtTokenService.getUsername(jwtTokenService.resolveToken(token))).getId())) {
                return new ResponseEntity<>(HttpStatus.OK);
            }
            new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @GetMapping("{id}/tracks/")
    public ResponseEntity<?> getTracks(@PathVariable Long id){
        User user = userService.read(id);
        if(user != null){
            return new ResponseEntity<>(user.getTracks(), HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<Track>(), HttpStatus.NOT_FOUND);
    }

    @GetMapping("{id}/chosentracks/")
    public ResponseEntity<?> getChosenTracks(@PathVariable Long id){
        User user = userService.read(id);
        if(user != null){
            return new ResponseEntity<>(user.getChosenTrack(), HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<Track>(), HttpStatus.NOT_FOUND);
    }
}
