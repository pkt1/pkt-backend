package com.testTrack.Tracks.WebSite.Requests;

import com.testTrack.Tracks.WebSite.entity.LocalObstacle;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestBody;

@Data
public class LocalObstacleCreateRequest {
    LocalObstacle localObstacle;
    Long id;
}
