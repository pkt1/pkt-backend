package com.testTrack.Tracks.WebSite.Requests;

import lombok.Data;

@Data
public class CategoryAnswer {
    int category;
}
