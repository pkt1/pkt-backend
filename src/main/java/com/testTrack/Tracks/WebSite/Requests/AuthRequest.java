package com.testTrack.Tracks.WebSite.Requests;

import lombok.Data;

@Data
public class AuthRequest {
    private String username;
    private String password;
}
