package com.testTrack.Tracks.WebSite.Requests;

import lombok.Data;
import org.springframework.web.bind.annotation.RequestBody;

@Data
public class TrackCreateRequest {
    String name;
    String cityName;
    String description;
}
