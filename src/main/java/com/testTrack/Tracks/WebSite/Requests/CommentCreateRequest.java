package com.testTrack.Tracks.WebSite.Requests;

import com.testTrack.Tracks.WebSite.entity.Comment;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestBody;

@Data
public class CommentCreateRequest {
    Comment comment;
    Long id;
}
