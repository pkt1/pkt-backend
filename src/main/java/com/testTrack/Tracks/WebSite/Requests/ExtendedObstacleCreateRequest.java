package com.testTrack.Tracks.WebSite.Requests;

import com.testTrack.Tracks.WebSite.entity.ExtendedObstacle;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestBody;

@Data
public class ExtendedObstacleCreateRequest {
    ExtendedObstacle extendedObstacle;
    Long id;
}
