package com.testTrack.Tracks.WebSite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TracksWebSiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(TracksWebSiteApplication.class, args);
	}

}
